<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Data Mahasiswa | Tambah Data</title>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
</head>
<body>
    <nav class="container mt-4 bg-priamry text-center">
        <h3>Tambah Data Mahasiswa</h3>
    </nav>
    <div class="container">
        <form action="/data" method="post">
            {{csrf_field()}}
            <div class="form-group">
                <label for="nama">Nama Lengkap</label>
                <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama Mahasiswa" autocomplete="off" autofocus>
            </div>
            <div class="form-group">
                <label for="nim">NIM</label>
                <input type="number" class="form-control" id="nim" name="nim" placeholder="NIM Mahasiswa" autocomplete="off">
            </div>
            <div class="form-group">
                <label for="kelas">Kelas Mahasiswa</label>
                <input type="text" class="form-control" id="kelas" name="kelas" placeholder="Kelas Mahasiswa">
            </div>
            <div class="form-group">
                <label for="prodi">Program Studi</label>
                <input type="text" class="form-control" id="prodi" name="prodi" placeholder="Program Studi Mahasiswa">
            </div>
            <div class="form-group">
                <label for="fakultas">Fakultas</label>
                <input type="text" class="form-control" id="fakultas" name="fakultas" placeholder="Fakultas Mahasiswa">
            </div>
            <button type="submit" class="btn btn-outline-success">Submit</button>
        </form>
    </div>
</body>
</html>