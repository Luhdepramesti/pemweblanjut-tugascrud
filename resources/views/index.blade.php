<!DOCTYPE html>
<html>
<head>
	<title>Data Mahasiswa Tahun Ajaran 2021</title>
	<link rel="stylesheet" href="/css/bootstrap.min.css">
</header>
</head>
<body>
	<div class="container ">
	<div align="center">
	<h1 class="text-center">Data Mahasiswa Tahun Ajaran 2021</h1>
	</div>
	<a href="/tambah"><button type="button" class="btn btn-outline-primary">+ Tambah Data Mahasiswa</button></a>
	
	<br/>
	<br/>
	<div align="center">
	<table class="table table-bordered table-striped" border="3">
		<tr>
            <th>Id</th>
			<th>Nama</th>
			<th>Nim</th>
			<th>Kelas</th>
			<th>Prodi</th>
            <th>Fakultas</th>
			<th>Option</th>
		</tr>
		@foreach($mahasiswa as $mhs)
		<tr>
            <td>{{ $mhs->id }}</td>
			<td>{{ $mhs->nama_mahasiswa }}</td>
			<td>{{ $mhs->nim_mahasiswa }}</td>
			<td>{{ $mhs->kelas_mahasiswa }}</td>
			<td>{{ $mhs->prodi_mahasiswa }}</td>
            <td>{{ $mhs->fakultas_mahasiswa }}</td>
			<td width="200px">
				<a href="/edit/{{$mhs->id}}"><button type="button" class="btn btn-outline-warning">Edit</button></a>
				<a href="/hapus/{{$mhs->id}}"><button type="button" class="btn btn-outline-danger">Delete</button></a>
			</td>
		</tr>
		@endforeach
	</table>
</body>
</html>